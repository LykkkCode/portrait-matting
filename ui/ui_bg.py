# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_bg.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGroupBox, QHBoxLayout, QLabel,
    QMainWindow, QPushButton, QSizePolicy, QVBoxLayout,
    QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QSize(800, 600))
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.originalBox = QGroupBox(self.centralwidget)
        self.originalBox.setObjectName(u"originalBox")
        sizePolicy.setHeightForWidth(self.originalBox.sizePolicy().hasHeightForWidth())
        self.originalBox.setSizePolicy(sizePolicy)
        self.originalBox.setMinimumSize(QSize(299, 589))
        self.originalBox.setMaximumSize(QSize(299, 589))
        self.verticalLayout = QVBoxLayout(self.originalBox)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.original_image = QLabel(self.originalBox)
        self.original_image.setObjectName(u"original_image")
        sizePolicy.setHeightForWidth(self.original_image.sizePolicy().hasHeightForWidth())
        self.original_image.setSizePolicy(sizePolicy)
        self.original_image.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.original_image)

        self.uploadButton = QPushButton(self.originalBox)
        self.uploadButton.setObjectName(u"uploadButton")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.uploadButton.sizePolicy().hasHeightForWidth())
        self.uploadButton.setSizePolicy(sizePolicy1)

        self.verticalLayout.addWidget(self.uploadButton, 0, Qt.AlignHCenter)


        self.horizontalLayout.addWidget(self.originalBox)

        self.bgColorButton = QPushButton(self.centralwidget)
        self.bgColorButton.setObjectName(u"bgColorButton")

        self.horizontalLayout.addWidget(self.bgColorButton)

        self.processedBox = QGroupBox(self.centralwidget)
        self.processedBox.setObjectName(u"processedBox")
        self.processedBox.setMinimumSize(QSize(299, 589))
        self.processedBox.setMaximumSize(QSize(299, 589))
        self.verticalLayout_2 = QVBoxLayout(self.processedBox)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.processed_image = QLabel(self.processedBox)
        self.processed_image.setObjectName(u"processed_image")
        self.processed_image.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.processed_image)

        self.saveButton = QPushButton(self.processedBox)
        self.saveButton.setObjectName(u"saveButton")
        sizePolicy1.setHeightForWidth(self.saveButton.sizePolicy().hasHeightForWidth())
        self.saveButton.setSizePolicy(sizePolicy1)

        self.verticalLayout_2.addWidget(self.saveButton, 0, Qt.AlignHCenter)


        self.horizontalLayout.addWidget(self.processedBox)

        self.horizontalLayout.setStretch(0, 2)
        self.horizontalLayout.setStretch(1, 1)
        self.horizontalLayout.setStretch(2, 2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"\u8bc1\u4ef6\u7167\u80cc\u666f\u66ff\u6362\u5de5\u5177\u5305", None))
        self.originalBox.setTitle(QCoreApplication.translate("MainWindow", u"\u539f\u56fe\u7247", None))
        self.original_image.setText(QCoreApplication.translate("MainWindow", u"\u5f85\u4e0a\u4f20\u56fe\u7247", None))
        self.uploadButton.setText(QCoreApplication.translate("MainWindow", u"\u4e0a\u4f20\u7167\u7247", None))
        self.bgColorButton.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u80cc\u666f\u989c\u8272", None))
        self.processedBox.setTitle(QCoreApplication.translate("MainWindow", u"\u56fe\u7247\u9884\u89c8", None))
        self.processed_image.setText(QCoreApplication.translate("MainWindow", u"\u9884\u89c8\u56fe\u7247", None))
        self.saveButton.setText(QCoreApplication.translate("MainWindow", u"\u4fdd\u5b58\u56fe\u7247", None))
    # retranslateUi

