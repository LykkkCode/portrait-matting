# 证件照背景替换

使用模型MODNet实现人脸抠图，对证件照背景进行替换。

模型来自于**柯章翰**

github仓库地址为[链接](https://github.com/ZHKKKe/MODNet)

- 可以通过添加背景图片给证件照更换背景
- 可以通过自定义rgb元组为证件照更换背景颜色

## 1. 直接运行py文件

### 项目运行

**测试图片来源于网络，侵删**

项目中`python`环境为3.8

**1.安装依赖**

```shell
pip install -r requirements.txt
```

**2.将背景图片放置于`background`文件夹下**

**3.将证件照图片放置于`input`文件夹下**

**4.修改`run_replace.py`中的代码**

```python
input_image_file = "portrait_2.png"
# background_image_file = "bg_2.jpg"
# 格式为(red, green, blue)
background_image_file = (255, 0, 0)
output_image_file = "output_rgb.jpg"
```

---------

### 最终实现效果

初始图片：

![portrait](./readme-images/portrait_2.png)

rgb图片效果：

![portrait](./readme-images/output_rgb.jpg)

自定义背景图片效果

![portrait](./readme-images/output_bg.jpg)

## 2. PySide6运行

安装好依赖后直接执行`main.py`文件。

**`Ui`界面展示**

![portrait](./readme-images/ui_image.png)

目前设置了修改背景颜色的功能，因为自己~~比较懒~~学业繁忙，有时间再把背景照片替换补上吧～

## 3. 背景透明化

想实现手写签名或者图标背景透明化然后嵌入到`word`或者`web`中，目前还在寻找本地项目部署实现的方法，在线网站可以去`https://www.remove.bg/`实现免费的背景透明化。

在开发中...
